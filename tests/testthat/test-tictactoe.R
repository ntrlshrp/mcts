test_that("multiplication works", {
  expect_equal(2 * 2, 4)
})



# game states
stateBlank <- data.frame(col1 = c(NA, NA, NA),
                         col2 = c(NA, NA, NA),
                         col3 = c(NA, NA, NA))

stateFirstMove <- data.frame(col1 = c(1, NA, NA),
                         col2 = c(NA, NA, NA),
                         col3 = c(NA, NA, NA))

stateSecondMove <- data.frame(col1 = c(1, NA, NA),
                             col2 = c(NA, -1, NA),
                             col3 = c(NA, NA, NA))

stateEndGame <- data.frame(col1 = c(1, 1, NA),
                           col2 = c(-1, -1, NA),
                           col3 = c(-1, 1, NA))

stateBlankMeta1s1 <- MCTS(
  playerID = 1,
  state0 = stateBlank,
  playSeconds = 1
)

newCellsState0Meta <-
  MCTS::MCTS(
    playerID = 2,
    state0 = stateFirstMove,
    state0meta = NULL,
    playSeconds = 1
  )
newCellsSequence <- c(NULL, 1)
newCellsSequence <- newCellsState0Meta$cell[newCellsState0Meta$n ==
  max(newCellsState0Meta$n[
    newCellsState0Meta$cell >= newCellsSequence * 10 &
      newCellsState0Meta$cell <= (newCellsSequence + 1) * 10
    ])][1]

MCTSPlot(state0meta = stateBlankMeta1s10,
         state0Seq = 0,
         player = 1)

stateEndGamemetaMCTS1s1 <- MCTS(
  playerID = 1,
  state0 = stateEndGame,
  playSeconds = 1
)

stateBlankmetaMCTS2s1 <- MCTS(
  playerID = 2,
  state0 = stateFirstMove,
  state0meta = stateBlankMeta1s1,
  playSeconds = 1
)

MCTSPlot(state0meta = stateBlankmetaMCTS2s1,
         state0Seq = 1,
         player = 2)

stateBlankmetaMCTS3s1 <- MCTS(
  playerID = 1,
  state0 = stateSecondMove,
  state0meta = stateBlankmetaMCTS2s1,
  playSeconds = 1
)

MCTSPlot(state0meta = stateBlankmetaMCTS3s1,
         state0Seq = 15,
         player = 1)



test_that("tictactoe EndGame works", {
  expect_equal(
    stateEndGamemetaMCTS1s1$V[
      stateEndGamemetaMCTS1s1$cell == 1243657 &
        stateEndGamemetaMCTS1s1$player == 1
      ], 1)

  expect_gte(
    stateEndGamemetaMCTS1s1$n[
      stateEndGamemetaMCTS1s1$cell == 1243657 &
        stateEndGamemetaMCTS1s1$player == 1
      ] /
    stateEndGamemetaMCTS1s1$n[
      stateEndGamemetaMCTS1s1$cell == 124365 &
        stateEndGamemetaMCTS1s1$player == 1
      ], 0.75)
})



test_that("tictactoe Blank works", {
  expect_gte(
    stateEndGamemetaMCTS1s1$V[
      stateEndGamemetaMCTS1s1$cell == 5 &
        stateEndGamemetaMCTS1s1$player == 1
      ], 0.3)

  expect_gte(
    stateEndGamemetaMCTS1s1$n[
      stateEndGamemetaMCTS1s1$cell == 5 &
        stateEndGamemetaMCTS1s1$player == 1
      ] /
      stateEndGamemetaMCTS1s1$n[
        stateEndGamemetaMCTS1s1$cell == 0 &
          stateEndGamemetaMCTS1s1$player == 1
        ], 0.3)
})



test_that("tictactoe combine works", {

})
